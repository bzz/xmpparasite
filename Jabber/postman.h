#ifndef network_h
#define network_h



#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>


#include "vdata.h"



#define PORT "5222"






//#include <assert.h>
//#include <errno.h>
//#include <stdarg.h>
#include <stdbool.h>
//#include <stdint.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
#include <time.h>
//#include <unistd.h>
//#include <signal.h>
#include <setjmp.h>
//#include <getopt.h>




#define MAXDATASIZE 100 // max number of bytes we can get at once

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}







#define push(buf, v)                                            \
do {                                                            \
grow_buf((buf), sizeof(v));                                     \
memcpy((buf)->data + (buf)->ptr, &(v), sizeof(v));              \
(buf)->ptr += sizeof(v);                                        \
} while (false)

#define NODE_NETWORK                1
#define USER_AGENT "/Satoshi:0.9.3/"

typedef int sock;
union uint256_s
{
    uint8_t i8[32];
    uint16_t i16[16];
    uint32_t i32[8];
    uint64_t i64[4];
};
typedef union uint256_s uint256_t;


struct header
{
    uint32_t magic;
    char command[12];
    uint32_t length;
    uint32_t checksum;
} __attribute__((__packed__));

struct block
{
    uint32_t version;
    uint256_t prev_block;
    uint256_t merkle_root;
    uint32_t timestamp;
    uint32_t bits;
    uint32_t nonce;
} __attribute__((__packed__));

struct buf
{
    struct peer *peer;
    char *data;
    uint32_t ptr;
    uint32_t len;
};

struct peer
{
    sock sock;
    uint32_t height;
    int32_t ref_count;
    struct buf *in_buf;
    struct buf *out_buf;
    time_t alive;
    struct in6_addr to_addr;
    in_port_t to_port;
    struct in6_addr from_addr;
    in_port_t from_port;
    char *name;
    uint32_t index;
    bool outbound;
    bool error;
    jmp_buf *env;
};

/*
static void make_version(struct peer *peer, uint64_t nonce, uint32_t height, bool use_relay)
{
    struct buf *buf = peer->out_buf;
    struct header hdr = {MAGIC, "version", 0, 0};
    push(buf, hdr);
    uint32_t version = PROTOCOL_VERSION;
    push(buf, version);
    uint64_t services = NODE_NETWORK;
    push(buf, services);
    uint64_t curr_time = time(NULL);
    push(buf, curr_time);
    push(buf, services);
    push(buf, peer->to_addr);
    push(buf, peer->to_port);
    push(buf, services);
    push(buf, peer->from_addr);
    push(buf, peer->from_port);
    push(buf, nonce);
    push_varstr(buf, USER_AGENT);
    push(buf, height);
}
*/

int networking_main()
{

    
    int sockfd, numbytes;
    char buf[MAXDATASIZE];
    struct addrinfo hints, *peeers_arr, *p;
    int rv;
    char s[INET6_ADDRSTRLEN];
    
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    

    if ((rv = getaddrinfo("xmpp.jp", PORT, &hints, &peeers_arr)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }
    
    for(p = peeers_arr; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                             p->ai_protocol)) == -1) {
            perror("client: socket");
            continue;
        }
        
        printf("%i\n", sockfd);
        
        if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            perror("client: connect");
            continue;
        }
        

        
        break;   //connect to first valid peer
    }
    
    
    
    
    
    if (p == NULL) {
        fprintf(stderr, "client: failed to connect\n");
        return 2;
    }
    
    inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
              s, sizeof s);
    printf("client: connecting to %s\n", s);
    
//    freeaddrinfo(peeers_arr);
    

    
    
    
    
    int spatula_count = PORT;
    vdata secret_message = vdata_from_hex("0b11090776657273696f6e0000000000620000009fd5c1617211010000000000000000000629ad5600000000010000000000000000000000000000000000ffffc39a7fec479d000000000000000000000000000000000000ffff7f000001479d8a22b4cac390824b0c2f77616c6c65743a312e302f0000000000");
    
    int stream_socket;
    struct sockaddr_in dest;
    int temp;
    
    // convert to network byte order
    temp = htonl(spatula_count);
    // send data normally:
    send(stream_socket, &temp, sizeof temp, 0);
    
    // send secret message out of band:
    send(stream_socket, secret_message.bytes, secret_message.len, MSG_OOB);

    
    
    
    
    
    
    
    
    if ((numbytes = recv(sockfd, buf, MAXDATASIZE-1, 0)) == -1) {
        perror("recv");
        exit(1);
    }
    
    buf[numbytes] = '\0';
    
    printf("client: received '%s'\n",buf);
    
    close(sockfd);
    
    return 0;
}




#endif /* network_h */
