

#import "iksemel/include/iksemel.h"



#include "common.h"
#include "iksemel.h"

#include <string.h>
#include <stdlib.h>


//#include "postman.h"




/* stuff we keep per session */
struct session {
   iksparser *prs;
   iksid *acc;
   char *pass;
   int features;
   int authorized;
   int counter;
   int set_roster;
   int job_done;
};

/* precious roster we'll deal with */
iks *my_roster;

/* out packet filter */
iksfilter *my_filter;

/* connection time outs if nothing comes for this much seconds */
int opt_timeout = 30;

/* connection flags */
int opt_use_tls;
int opt_use_sasl;
int opt_use_plain;
int opt_log;

void
j_error (char *msg)
{
   fprintf (stderr, "iksroster: %s\n", msg);
   exit (2);
}

int
on_result (struct session *sess, ikspak *pak)
{
   iks *x;
   
   if (sess->set_roster == 0) {
      x = iks_make_iq (IKS_TYPE_GET, IKS_NS_ROSTER);
      iks_insert_attrib (x, "id", "roster");
      iks_send (sess->prs, x);
      iks_delete (x);
   } else {
      iks_insert_attrib (my_roster, "type", "set");
      iks_send (sess->prs, my_roster);
   }
   return IKS_FILTER_EAT;
}


char* prepare_auth_PLAIN(char* login, char* password) {
   size_t login_len = strlen(login);
   size_t password_len = strlen(password);
   char *plain= calloc(100,1);
   char *ii = plain;
   ++ii;
   memcpy(ii, login, login_len);
   ii += login_len + 1;
   memcpy(ii, password, password_len);
   return iks_base64_encode(plain, login_len + password_len + 2);
}


int
on_stream (struct session *sess, int type, iks *node)
{
   sess->counter = opt_timeout;
   
   switch (type) {
      case IKS_NODE_START:
         if (opt_use_tls && !iks_is_secure (sess->prs)) {
            iks_start_tls (sess->prs);
            break;
         }
         
         char *msg = malloc(1000);
         char* login_password = prepare_auth_PLAIN(sess->acc->partial, sess->pass);
         sprintf(msg, "<auth xmlns='urn:ietf:params:xml:ns:xmpp-sasl' mechanism='PLAIN'>%s=</auth>", login_password);
         iks_send_raw (sess->prs, msg);
         
         
         //            if (!opt_use_sasl) {
         //                iks *x;
         //                char *sid = NULL;
         //
         //                if (!opt_use_plain) sid = iks_find_attrib (node, "id");
         //                x = iks_make_auth (sess->acc, sess->pass, sid);
         //                iks_insert_attrib (x, "id", "auth");
         //
         //
         //
         //                iks_send (sess->prs, x);
         //                iks_delete (x);
         //            }
         break;
         
      case IKS_NODE_NORMAL:
         if (strcmp ("stream:features", iks_name (node)) == 0) {
            sess->features = iks_stream_features (node);
            if (opt_use_sasl) {
               if (opt_use_tls && !iks_is_secure (sess->prs)) break;
               if (sess->authorized) {
                  iks *t;
                  if (sess->features & IKS_STREAM_BIND) {
                     t = iks_make_resource_bind (sess->acc);
                     iks_send (sess->prs, t);
                     iks_delete (t);
                  }
                  if (sess->features & IKS_STREAM_SESSION) {
                     t = iks_make_session ();
                     iks_insert_attrib (t, "id", "auth");
                     iks_send (sess->prs, t);
                     iks_delete (t);
                  }
               } else {
                  if (sess->features & IKS_STREAM_SASL_MD5)
                     iks_start_sasl (sess->prs, IKS_SASL_DIGEST_MD5, sess->acc->user, sess->pass);
                  else if (sess->features & IKS_STREAM_SASL_PLAIN)
                     iks_start_sasl (sess->prs, IKS_SASL_PLAIN, sess->acc->user, sess->pass);
               }
            }
         } else if (strcmp ("failure", iks_name (node)) == 0) {
            j_error ("sasl authentication failed");
         } else if (strcmp ("success", iks_name (node)) == 0) {
            sess->authorized = 1;
            iks_send_header (sess->prs, sess->acc->server);
         } else {
            ikspak *pak;
            
            pak = iks_packet (node);
            iks_filter_packet (my_filter, pak);
            if (sess->job_done == 1) return IKS_HOOK;
         }
         break;
         
      case IKS_NODE_STOP:
         j_error ("server disconnected");
         
      case IKS_NODE_ERROR:
         j_error ("stream error");
   }
   
   if (node) iks_delete (node);
   return IKS_OK;
}

int
on_error (void *user_data, ikspak *pak)
{
   j_error ("authorization failed");
   return IKS_FILTER_EAT;
}

int
on_roster (struct session *sess, ikspak *pak)
{
   my_roster = pak->x;
   sess->job_done = 1;
   return IKS_FILTER_EAT;
}

void
on_log (struct session *sess, const char *data, size_t size, int is_incoming)
{
   if (iks_is_secure (sess->prs)) fprintf (stderr, "Sec");
   if (is_incoming) fprintf (stderr, "RECV"); else fprintf (stderr, "SEND");
   fprintf (stderr, "[%s]\n", data);
}

void
j_setup_filter (struct session *sess)
{
   if (my_filter) iks_filter_delete (my_filter);
   my_filter = iks_filter_new ();
   iks_filter_add_rule (my_filter, (iksFilterHook *) on_result, sess,
                        IKS_RULE_TYPE, IKS_PAK_IQ,
                        IKS_RULE_SUBTYPE, IKS_TYPE_RESULT,
                        IKS_RULE_ID, "auth",
                        IKS_RULE_DONE);
   iks_filter_add_rule (my_filter, on_error, sess,
                        IKS_RULE_TYPE, IKS_PAK_IQ,
                        IKS_RULE_SUBTYPE, IKS_TYPE_ERROR,
                        IKS_RULE_ID, "auth",
                        IKS_RULE_DONE);
   iks_filter_add_rule (my_filter, (iksFilterHook *) on_roster, sess,
                        IKS_RULE_TYPE, IKS_PAK_IQ,
                        IKS_RULE_SUBTYPE, IKS_TYPE_RESULT,
                        IKS_RULE_ID, "roster",
                        IKS_RULE_DONE);
}


//
//  Jabberman.m
//  Jabber
//
//  Created by Mikhail Baynov on 16/03/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

#import "Jabberman.h"

@interface Jabberman () {
   NSMutableData *sessionData;
   struct session sess;
}
@end



@implementation Jabberman


- (void)connect {
   
   
   char* jabber_id = "bzz@xmpp.jp";
   char* pass = "134679";
   int set_roster = 0;
   
   
   
   //new session
   memset (&sess, 0, sizeof (sess));
   sess.prs = iks_stream_new (IKS_NS_CLIENT, &sess, (iksStreamHook *) on_stream);

   opt_log = 1;
   
   if (opt_log) iks_set_log_hook (sess.prs, (iksLogHook *) on_log);
   sess.acc = iks_id_new (iks_parser_stack (sess.prs), jabber_id);
   if (NULL == sess.acc->resource) {
      /* user gave no resource name, use the default */
      char *tmp;
      tmp = iks_malloc (strlen (sess.acc->user) + strlen (sess.acc->server) + 9 + 3);
      sprintf (tmp, "%s@%s/%s", sess.acc->user, sess.acc->server, "iksroster");
      sess.acc = iks_id_new (iks_parser_stack (sess.prs), tmp);
      iks_free (tmp);
   }
   sess.pass = pass;
   sess.set_roster = set_roster;
   
   j_setup_filter (&sess);
   
   
   
   sessionData = [[NSMutableData alloc]initWithBytes:&sess length:sizeof(sess)];
   
   
   
   //connect
   int e = iks_connect_with (sess.prs, sess.acc->server, IKS_JABBER_PORT, sess.acc->server, &iks_default_transport);
   switch (e) {
      case IKS_OK:
         [self.delegate jabberConnected:[NSString stringWithCString:sess.acc->server encoding:NSUTF8StringEncoding]];
         break;
      case IKS_NET_NODNS:
         j_error ("hostname lookup failed");
      case IKS_NET_NOCONN:
         j_error ("IKS_NET_NOCONN: connection failed");
      default:
         j_error ("io error");
   }
   
   sess.counter = opt_timeout;
   while (1) {
      
      
      int timeout = 1;
      
      struct stream_data *data = iks_user_data (sess.prs);
      int len, ret;
      
      printf("session counter %i\n", sess.counter);
      while (1) {
         if (data->flags & SF_SECURE) {
            len = iks_default_tls.recv(data->tlsdata, data->buf, NET_IO_BUF_SIZE - 1, timeout);
         } else {
            len = data->trans->recv(data->sock, data->buf, NET_IO_BUF_SIZE - 1, timeout);
         }
         if (len < 0) e = IKS_NET_RWERR;
         if (len == 0) break;
         
         data->buf[len] = '\0';
         if (data->logHook) data->logHook (data->user_data, data->buf, len, 1);
         
         [self.delegate jabberGotResponse:[NSString stringWithCString:data->buf encoding:NSUTF8StringEncoding]];
         
         
         ret = iks_parse (sess.prs, data->buf, len, 0);
         if (ret != IKS_OK) e = ret;
         if (!data->trans)  e = IKS_NET_NOCONN; /* stream hook called iks_disconnect */
         
         

         
         timeout = 0;
      }
      
      
      
      
      
      
      
      if (IKS_HOOK == e) break;
      if (IKS_NET_TLSFAIL == e) j_error ("tls handshake failed");
      if (IKS_OK != e) j_error ("io error");
      sess.counter--;
      if (sess.counter == 0) {
         [self.delegate networkTimeout];
         j_error ("network timeout");
      }
   }
   
   
   //cleanup
   iks_parser_delete (sess.prs);
}


- (void)send:(NSString *)message {
   iks_send_raw (sess.prs, message.UTF8String);
}




@end
