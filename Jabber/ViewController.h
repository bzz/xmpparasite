//
//  ViewController.h
//  Jabber
//
//  Created by Mikhail Baynov on 11/03/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jabberman.h"


@interface ViewController : UIViewController <JabberDelegate>



@end

