#ifndef vdata_h
#define vdata_h


/* allocates len bytes for new vdata */
#define VDATA_NEW( len )  {len, calloc(len, 1)}

typedef struct {
   size_t len;
   uint8_t *bytes;
} vdata;



#warning make it btc_hash256_t
#define BTC_HASH160_LEN 20
typedef struct {
   uint8_t bytes[BTC_HASH160_LEN];
} btc_hash160;

#define BTC_HASH256_LEN 32
typedef struct {
   uint8_t bytes[32];
} btc_hash256_t;



typedef struct {
   uint64_t balance;
   int32_t index;
   vdata id;
} btc_txinput;

typedef struct {
   uint64_t amount;
   btc_hash160 hash160;
} btc_txoutput;





void vdata_print_hex(const char* name, const vdata data)
{
   printf("%s (%zu): ", name, data.len);
   if (data.bytes) {
      for (size_t i = 0; i < data.len; ++i) {
         printf("%02x", data.bytes[i]);
      }
   } else printf("[empty]");
   printf("\n");
}


char* vdata_get_hex(const vdata data)
{
   char hex_str[]= "0123456789abcdef";

   char* out;
   out = (char *)malloc(data.len * 2 + 1);
   (out)[data.len * 2] = 0;
   
   if (!data.len) return NULL;
   
   for (size_t i = 0; i < data.len; i++) {
      (out)[i * 2 + 0] = hex_str[(data.bytes[i] >> 4) & 0x0F];
      (out)[i * 2 + 1] = hex_str[(data.bytes[i]     ) & 0x0F];
   }
   return out;
}


void bin_to_strhex(uint8_t *bin, size_t binsz, char **result)
{
   char hex_str[]= "0123456789abcdef";
   unsigned int i;
   
   *result = (char *)malloc(binsz * 2 + 1);
   (*result)[binsz * 2] = 0;
   
   if (!binsz)
      return;
   
   for (i = 0; i < binsz; i++)
   {
      (*result)[i * 2 + 0] = hex_str[(bin[i] >> 4) & 0x0F];
      (*result)[i * 2 + 1] = hex_str[(bin[i]     ) & 0x0F];
   }  
}






/** init vdata from serialized message */
vdata vdata_parse(uint8_t *in)
{
   vdata out;
   if (in) {
      if (in[0] == 0xfd) {
         out.len =
         (in[1] << 8) +
         in[2];
         out.bytes = &in[3];
      }
      else if (in[0] == 0xfe) {
         out.len =
         (in[1] << 24) +
         (in[2] << 16) +
         (in[3] << 8) +
         in[4];
         out.bytes = &in[5];
      }
      else if (in[0] == 0xff) {
         ///maximum value of uint64_t is 0xffffffffffffffff
         ///maximum value of size_t   is 0xffffffff
         out.len = (in[1]+in[2]+in[3]+in[4] > 0)? 0xffffffff :
         (in[5] << 24) +
         (in[6] << 16) +
         (in[7] << 8) +
         in[8];
         out.bytes = &in[9];
      }
      else {
         out.len = (uint8_t)in[0];
         out.bytes = &in[1];
      }
   }
   
   return out;
}


/*get serialized message*/
uint8_t* vdata_get_serialized_message(uint8_t *data, size_t len)
{
   //   vardata in;
   //   in.data = data;
   //   in.len = len;
   
   uint8_t *out;
   if (len < 0xfd) {
      out = malloc(0xff + 1);
      out[0] = len;
      memcpy(out+1, data, len);
   } else if (len >= 0xfd && len <= 0xffff) {
      out = malloc(len + 3);
      out[0] = 0xfd;
      out[1] = len >> 8;
      out[2] = len;
      memcpy(out+3, data, len);
   } else if (len > 0xffff && len <= 0xffffffff) {
#warning maybe use memmove instead of memcpy?
      out = malloc(len + 5);
      out[0] = 0xfe;
      out[1] = len >> 24;
      out[2] = len >> 16;
      out[3] = len >> 8;
      out[4] = len;
      memcpy(out+5, data, len);
   } //else?
   return out;
}


/*serialize vdata*/
uint8_t* vdata_serialize(vdata data) {
   return vdata_get_serialized_message(data.bytes, data.len);
}


/*init vdata from bytes*/
vdata vdata_from_bytes(uint8_t *data, size_t len) {
   return vdata_parse(vdata_get_serialized_message(data, len));
}


uint8_t bbp_hex2byte(const char ch) {
   if ((ch >= '0') && (ch <= '9')) {
      return ch - '0';
   }
   if ((ch >= 'a') && (ch <= 'f')) {
      return ch - 'a' + 10;
   }
   return 0;
}
/** init vdata from hex*/
vdata vdata_from_hex(const char *str) {
   const size_t count = strlen(str) / 2;
   uint8_t out[count];
   for (size_t i = 0; i < count; ++i) {
      const char hi = bbp_hex2byte(str[i * 2]);
      const char lo = bbp_hex2byte(str[i * 2 + 1]);
      out[i] = hi * 16 + lo;
   }
   return vdata_from_bytes(out, count);
}


/** init vdata from base58*/
vdata vdata_from_base58(const char *str) {
   const size_t len = strlen(str);

   size_t i = 0;
   size_t z = 0;
   
   while (z < len && !str[z]) ++z; // count leading zeroes
   
  
   uint8_t buf[(len - z)*733/1000 + 1]; // log(58)/log(256), rounded up
   
   memset(buf, 0, sizeof(buf));
   
   for (i = z; i < len; ++i) {
      uint32_t carry = str[i];// [self characterAtIndex:i];
      
      switch (carry) {
         case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
            carry -= '1';
            break;
            
         case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H':
            carry += 9 - 'A';
            break;
            
         case 'J': case 'K': case 'L': case 'M': case 'N':
            carry += 17 - 'J';
            break;
            
         case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y':
         case 'Z':
            carry += 22 - 'P';
            break;
            
         case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j':
         case 'k':
            carry += 33 - 'a';
            break;
            
         case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v':
         case 'w': case 'x': case 'y': case 'z':
            carry += 44 - 'm';
            break;
            
         default:
            carry = UINT32_MAX;
      }
      
      if (carry >= 58) break; // invalid base58 digit
      
      for (size_t j = sizeof(buf); j > 0; --j) {
         carry += (uint32_t)buf[j - 1]*58;
         buf[j - 1] = carry & 0xff;
         carry >>= 8;
      }
      
      memset(&carry, 0, sizeof(carry));
  }

   i = 0;
   while (i < sizeof(buf) && buf[i] == 0) i++; // skip leading zeroes
   
   return vdata_from_bytes(buf, sizeof(buf) - i);
}



/** init vdata from range of vdata*/
vdata vdata_get_range(vdata data, size_t start_index, size_t len) {
   if (len > data.len - start_index) len = data.len - start_index;   //truncate to valid size
   uint8_t *ptr = data.bytes;
   ptr += start_index;
   data.bytes = ptr;
   data.len = len;
//   vdata_print_hex("btc_ophash from msKxGtZDoXeWjnxPNJbvZCBmLVv1fNqQMd", data);
   return data;
}


int is_vdata_equal(vdata x, vdata y) {
   if (x.len != y.len) return 0;
   for (size_t i=0; i<x.len; ++i) {
      if (x.bytes[i] != y.bytes[i]) return 0;
   }
   return 1;
}




const char* btc_ophash160_hex_from_base58check(const char *str) {
#warning ADD BASE58CHECK VALIDITY CHECK!
   if (strlen(str) < 4) return NULL;
   vdata in = vdata_from_base58(str);
   
   vdata out = vdata_get_range(in, 1, in.len - 5);
   return vdata_get_hex(out);
}


btc_hash160 btc_hash160_from_addr(const char *str) {
#warning ADD BASE58CHECK VALIDITY CHECK!
   btc_hash160 out = {0};
   if (strlen(str) < 4) return out;
   vdata in = vdata_from_base58(str);
   in = vdata_get_range(in, 1, in.len - 5);
   if (in.len !=20) return out;
   memcpy(out.bytes, in.bytes, 20);
   return out;
}





#endif /* vdata_h */
