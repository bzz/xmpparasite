//
//  Jabberman.h
//  Jabber
//
//  Created by Mikhail Baynov on 16/03/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//


#import <Foundation/Foundation.h>


@class Jabberman;

@protocol JabberDelegate <NSObject>

- (void)jabberConnected:(NSString *)server;
- (void)jabberGotResponse:(NSString *)response;
- (void)networkTimeout;

@end




@interface Jabberman : NSObject {}

@property (nonatomic, weak) id <JabberDelegate> delegate; //define MyClassDelegate as delegate

- (void)connect;
- (void)send:(NSString *)message;

@end
